//Guarda datos de temperatura y humedad maximos y minimos--> valores t, tmax, tmin, h, hmax y hmin
//Resetea el horacontador de la funcion graficas_24h un minuto antes de las 24h 00:00h
void temperatura () 
{
  int htemp = h;
  h = dht.readHumidity();
  if (  h < 100  ) {  
    if (hora == 23  && minuto  == 59 ) {
      hmax = -1;  //Reseteamos la humedad maxima y minima a la hora 00:00 --> la humedad no puede ser mayor del 100% ni menor de 0%
      hmin = 101;
      horacontador = 0;
    }
    if  ( hmax  <  h  ) {      hmax  = h;    }
    if  ( hmin  > h ) {     hmin  = h;    }
  }
  else
  {
    h = htemp;
  }

  int ttemp =  t;
  t = dht.readTemperature();
  if (t < 300)
  {
    if (hora == 23  && minuto  == 59 ) {
      tmax = -100;  //Reseteamos la temperatura maxima y minima a la hora 00:00
      tmin = 300;
    }
    if (tmax < t) {
      tmax = t; //Si la temperatura maxima es menor de temperatura actual guarda la nueva temperatura maxima registrada
    }
    if (tmin > t) {
      tmin = t; //Si la temperatura minima es mayor de temperatura actual guarda la nueva temperatura minima
    }
  }
  else
  {
    t = ttemp;
  }
}
