
void geoip()
{
  const char* host = "freegeoip.net";
//  Serial.print("connecting to ");
//  Serial.println(host); 


// Use WiFiClient class to create TCP connections
  WiFiClient client;
  const int httpPort = 80;
  if (!client.connect(host, httpPort)) {
    Serial.println("connection failed");
    return;
  }
  
// We now create a URI for the request
  String url = "/json/";
  
//  Serial.print("Requesting URL: ");
//  Serial.println(url);
  
// This will send the request to the server
  client.print(String("GET ") + url + " HTTP/1.1\r\n" +
               "Host: " + host + "\r\n" + 
               "Connection: close\r\n\r\n");
  unsigned long timeout = millis();
  while (client.available() == 0) {
    if (millis() - timeout > 15000) {
      Serial.println(">>> Client Timeout !");
      client.stop();
      return;
    }
  }
//  Serial.print("Remote IP ");
//  Serial.println( client.remoteIP() );
//   Serial.print("Local IP ");
//  Serial.println( client.localIP() ); 
//  Serial.print("");

  
// Read all the lines of the reply from server and print them to Serial
  byte dns_c = 0;
  char dns[255];
  String json;
  while(client.available())
  {
     json = client.readString();
     //json += client.readStringUntil('\n');
  }
  int Bracket = json.indexOf('{');
  int Bracket_end = json.indexOf('}');
  
  json.substring(Bracket, Bracket_end +1 ).toCharArray(dns,255);
//  Serial.println("");
//  Serial.println("Subtring******************* ");
//  Serial.println( json.substring(Bracket, Bracket_end +1 )  );
//  Serial.println("");
//  Serial.println("Json******************* ");
//  Serial.println(json);
//
//  Serial.println("Dns******************");
//  Serial.println(String(dns));
//  Serial.println("closing connection");
  setObject (dns);
}


void setObject(char ojb[255])
{
StaticJsonBuffer<200> jsonBuffer;

char json[200];
//json = ojb;
JsonObject& root = jsonBuffer.parseObject(ojb);

  // TEST
  if (!root.success()) {
    Serial.println("parseObject() failed");
    return;
  }

  // In other case, you can do root["time"].as<long>();
  const char* ip = root["ip"];
  const char* about = root["country_code"];
  const char* pro = root["city"];
  const char* reject = root["time_zone"];


  // Print values.
  Serial.println("");
  Serial.println(ip);
  Serial.println(about);
  Serial.println(pro);
  Serial.println(reject);
}


