//WebAPP envia datos y esta funcion los procesa y manipula esos datos--> funcion padre de ram_lectura()
void recuperardatos_Ram_App_1()
{
  if(server.arg("i") == "m")
  {
    appmodo();
  }
  
  else if(server.arg("i") == "r")
  {
    apptabla();
  }
  
  else if( server.arg("i") == "w" )
  {
    appwifi( server.arg("ssid"), server.arg("pass" ) ); 
  }
  
  else if( server.arg("i") == "f"  )
  {
    appflag(server.arg("zone"), server.arg("flag") );
  }

  else if( server.arg("i") == "i"  )
  {
    appidioma(  server.arg("idioma")  );
  }
  
  else if( server.arg("i") == "n"  )
  {
    appnombre(  server.arg("nombre")  );
  }

  else if( server.arg("i") == "a"  )
  {
    appauth(  server.arg("user"), server.arg("pass")  );
  }
  
  else if ( server.arg("i") == "g"  )
  {
    app_guardar ();
  }
  
}

void app_guardar()
{
  almacen ();
  guardareeprom ();
//  Serial.println("");
//  Serial.print("Cache despues de guardar: ");
//  Serial.println(cache);
}

void appwifi (String appssid, String apppass)
{
  WifiSSID =  appssid;
  WifiPASS =  apppass;
}
void appauth (String appuser, String apppass)
{
  IomUSER = appuser;
  IomPASS = apppass;
}

void appidioma (String app_idioma)
{
  IDIOMA = app_idioma;
}

void appnombre ( String app_nombre)
{
  NombreIOM = app_nombre;
}

void appflag ( String app_zone, String app_flag)
{

        timeZone = app_zone.toInt();
        
        if ( app_flag == 0) 
        {
          flag = false;
        }
        else  {flag = true;}
        
}

void appmodo()
{
  
    byte x = 0;
    for ( byte i = 3; i <= 7; i++ )
    {
     modo[x] = server.arg(i).toInt(); //i-x = 0
//     Serial.println("");
//     Serial.print("modo: ");
//     Serial.println( modo[x] );
     x++;
    }
}

void apptabla ()
{
        byte x = 0;
        for ( byte i = 3; i <= 18; i++  )
        {
          r[x] = server.arg(i).toInt(); //i-x = 1 --> 2-1=1
//               Serial.println("");
//               Serial.print("r: ");
//               Serial.println( r[x] );
          x++;
        }
          
}
void recuperardatos_Ram_App() 
{
    memset(ram, 0, 100);
    server.uri().toCharArray(ram, sizecadena );
    //Serial.print("url: "); Serial.println(server.uri());
    //Serial.print("Lectura: "); Serial.println( ram_lectura(1) );
    int y = ram_lectura(3).toInt();

  switch ( y )
  {

    case 1: //WebApp envia datos booleanos de la configuración de cada relé I/O (encendido/apagado) a Wemos por http y Wemos envia esta cadena por tx/rx a arduino uno
      {

          byte x = 0;
          for ( byte i = 4; i <= 8; i++ )
          {
            modo[x] = ram_lectura(i).toInt(); //i-x = 0
            Serial.print( modo[x] );
            x++;
          }
        salidas();
        //guardareeprom ();
        
      }
      break;
      
    case 2: //WebApp envia datos numéricos de la configuración de cada range (encendido/apagado) a Wemos por http y Wemos envia esta cadena por tx/rx a arduino uno
      {
        byte x = 0;
        for ( byte i = 4; i <= 19; i++  )
        {
          r[x] = ram_lectura(i).toInt(); //i-x = 1 --> 2-1=1
          Serial.print( r[x] );
          x++;
        }
        salidas();
        //guardareeprom ();        
      }
      break;
      
    case 3://WebApp envia datos Configuración de Red Wifi del Router a Wemos por http y Wemos envia esta cadena por tx/rx a arduino uno que guarda el nuevo cambio en la e2prom
      {
//        WifiSSID ="";
//        WifiPASS = "";
//        WifiSSID = ram_lectura(4);
//        WifiPASS = ram_lectura(5);
        modo[0] = ram_lectura(6).toInt();
        //Serial.print("ssid: "); Serial.println( ram_lectura(4)  );
        //Serial.print("ssid: "); Serial.println( ram_lectura(5)  );
        //guardareeprom ();
        delay(300);
        //ESP.restart();
      }
      break;
    case 4://WebApp envia datos Configuración inicio de sesion Moviola a Wemos por http y Wemos envia esta cadena por tx/rx a arduino uno que guarda el nuevo cambio en la e2prom
      {
        IomUSER = ram_lectura(4);
        IomPASS = ram_lectura(5);
        //guardareeprom ();
        //Sistema de autenticidad - if  (UserActualArduino == UserActualApp) { UserActualArduino = UserNuevoApp; }
      }
      break;
    case 5://WebApp envia datos sobre la bandera 0 o 1 a Wemos por http y Wemos envia esta cadena por tx/rx a arduino uno que guarda el nuevo cambio en la e2prom
      {
        FLAG = ram_lectura(4);
        //e2prom_grabar();
        //e2prom_a_spi();
        //guardareeprom ();

      }
      break;
    case 6:
      {
        ID_IOM = ram_lectura(4);//ID del dispositivo = ID_Unique_Mysql; -->funcion que guarda el ID unico generado para este disposito desde MySql
        //guardareeprom ();
      }
      break;
    case 7:
      {
        int novahora = ram_lectura(4).toInt();
        int novaminuto = ram_lectura(5).toInt();
        //rtc.adjust(DateTime(2016, 10, 25, novahora, novaminuto, 0));
        //modicar por el nuevo codigo ntp
      }
      break;
    case 8:
      {
        NombreIOM = ram_lectura(4);
       //Serial.print("Nombre cambiado: "); Serial.println(NombreIOM);
        //Wifiap = NombreIOM;
        //guardareeprom ();
      }
      break;
    case 9:
      {
        IDIOMA = ram_lectura(4);
        //guardareeprom ();
      }
      break;
    default:
      break;
  }
   //memset borra el contenido del array  "cadena" desde la posición 0 hasta el final--> sizeof(ram) el tamaño de ram marca el final
  // Logica para determinar cuando guardar en memoria intermedia ram
  //Logica para determinar cuando guardar en cache y despues en e2prom.
}
