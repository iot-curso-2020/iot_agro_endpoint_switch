
void s_xml() {

  String  xml  = F ( ""  );
  xml += F ("<?xml version = \"1.0\" ?>\n");
  xml += F ("<inputs>\n"); //imprimimos la etiqueta

  // TEMPERATURA & HUMEDAD
  xml += F ("<TEMPERATURA>");
  xml += (t);
  xml += F ("</TEMPERATURA>\n");
  xml += F ("<TMAX>");
  xml += (tmax);
  xml += F ("</TMAX>\n");
  xml += F ("<TMIN>");
  xml += (tmin);
  xml += F ("</TMIN>\n");
  xml += F ("<HUMEDAD>");
  xml += (h);
  xml += F ("</HUMEDAD>\n");
  xml += F ("<HMAX>");
  xml += (hmax);
  xml += F ("</HMAX>\n");
  xml += F ("<HMIN>");
  xml += (hmin);
  xml += F ("</HMIN>\n");
  // HORA & MINUTO
  xml += F ("<HORA>");
  if (hora < 10 )//si Hora es menor de 10 se le coloca un cero delante
  {
    String cachehora = String(hora);
    String XMLhora = "";
    String cero = "0";
    XMLhora = cero + cachehora;
    xml += (XMLhora);
  }
  else
  {
    xml += (hora);
  }
  xml += F ("</HORA>\n");
  xml += F ("<MINUTO>");
  if (minuto < 10 )//si Hora es menor de 10 se le coloca un cero delante
  {
    String cacheminuto = String(minuto);
    String XMLminuto = "";
    String cero = "0";
    XMLminuto = cero + cacheminuto;
    xml += (XMLminuto);
  }
  else
  {
    xml += (minuto);
  }
  xml += F ("</MINUTO>\n");
  // IDIOMA
  xml += F ("<IDIOMA>");
  xml += (IDIOMA);
  xml += F ("</IDIOMA>\n");
  // NOMBRE_IOM
  xml += F ("<NOMBRE>");
  xml += (NombreIOM);
  xml += F ("</NOMBRE>\n");
  // IP & WIFISSID
  xml += F ("<IP>");
  xml += IP;
  xml += F ("</IP>\n");
  xml += F ("<WIFISSID>");
  xml += (Wifiap);
  xml += F ("</WIFISSID>\n");

  xml += F ("</inputs>");

  //Poner hora y minuto en la interface <HORA></HORA y <MINUTO></MINUTO>
  server.send ( 200 , "text/xml" , xml  );
  //Serial.println(xml);

}
