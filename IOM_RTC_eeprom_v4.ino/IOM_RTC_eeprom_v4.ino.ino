
/*
  Autor: @fenixbinario
  Compañia: Moviola S.A
  Control de Versiones:
  FUENTE ALIMENTACIÓN 12V 2A + PCB_4_RELES + IOM_RTC_v2.ino/ Wemos +[ RT / RX ]+ lcd_liquid.ino/ArduinoUno + LCD_LIQUID_16X2
  WEMOS Y ARDUINO_UNO--> CONECTADO POR TX / RX
  WEMOS --> 4 RELES [ D0, D1, D2, D6 ]
  WEMOS --> SENSOR DTH11 [ D6 ]
  ARDUINO_UNO Y LCD_LIQUID_16X2 --> lcd(7, 8, 9, 10, 11, 12);
*/
#include <ArduinoJson.h>
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <Arduino.h>
#include <EEPROM.h>
#include <TimeLib.h>
//#include <Wire.h> 
//#include "RTClib.h"
#include <DHT.h>
// NTP:
//IPAddress timeServer(132, 163, 4, 101); // time-a.timefreq.bldrdoc.gov   
  IPAddress timeServer(130, 206, 3, 166); // hora.rediris.es  130.206.3.166
// IPAddress timeServer(132, 163, 4, 102); // time-b.timefreq.bldrdoc.gov
// IPAddress timeServer(132, 163, 4, 103); // time-c.timefreq.bldrdoc.gov

 int timeZone;     // Central European Time
//const int timeZone = -5;  // Eastern Standard Time (USA)
//const int timeZone = -4;  // Eastern Daylight Time (USA)
//const int timeZone = -8;  // Pacific Standard Time (USA)
//const int timeZone = -7;  // Pacific Daylight Time (USA)

// UDP:
WiFiUDP Udp;

unsigned int localPort = 8888;  // local port to listen for UDP packets

//SERVIDOR WEB
ESP8266WebServer server ( 1031 ); //1031
IPAddress myIP;
//JSON


//SENSORES
#define DHTPIN D5
#define DHTTYPE DHT11 //  Este sensor recoge datos entre los 0ºC y los 50ºC
DHT dht(DHTPIN, DHTTYPE);
int t ;
int h ;
int tmax = -1;
int tmin = 51;
int hmax = -1;
int hmin = 101;
unsigned long time_spi = 1000; //usado para enviar cada un minuto un string por SPI

//RTC
int hora;
int minuto;
int segundo;
//RTC_Millis rtc;

//GRAFICA
int horaactual;
int horacontador = 0;
int cachetemp [23];
int cachehum [23];
int cachehora [23];

//DECLARAR VARIABLES QUE TOMAN SU VALOR DESDE LA MEMORIA EPROM
String ID_IOM ;
String NombreIOM ;
String Wifiap;
String WifiSSID;
String WifiPASS;
String IomUSER ;
String IomPASS;
String IDIOMA ;
boolean flag; //Memoria principal False por defecto desde UNO --> activa la propia wifi-- recuperardatos_ram_eprom()

String FLAG; //

int modo [4];
int r[16];
//Declarar variables intermedias para volcar información
#define sizecadena 512
char cadena [sizecadena];  //desde e2prom hacia fuera-->leer e2prom
String cache; //desde fuera hacia e2prom-->guardar e2prom
String ram_eeprom;
String eeprom[35];
char ram [sizecadena];

String IP;

//CONFIGURAR WIFI



//char ssid [31];
//char password [63];



//CONFIGURAR 4 SALIDAS
byte rele [] = {16,5,4,12};
byte rele_0 = D0;
byte rele_1 = D1;
byte rele_2 = D2;
byte rele_3 = D6;

void handleNotFound() {
//    String message = "File Not Found\n\n";
//    message += "URI: ";
//    message += server.uri();
//    message += "\nMethod: ";
//    message += ( server.method() == HTTP_GET ) ? "GET" : "POST";
//    message += "\nArguments: ";
//    message += server.args();
//    message += "\n";
//    //message += HTTP_GET;//elininar esta linea
//  
//    for ( uint8_t i = 0; i < server.args(); i++ ) {
//      message += " " + server.argName ( i ) + ": " + server.arg ( i ) + "\n";
//    }
//    Serial.print("Archivo Denegado:  ");
//    Serial.println(server.uri());
//    server.send ( 404, "text/plain", message );
  //server.send ( 200, "text/plain", "ok" );
  recuperardatos_Ram_App();

}


void setup()
{
  
//Cada vez que se reinicie que actualice el reloj desde internet
//flag = FLAG; convertir un string a bolean
  Serial.begin(115200);
  server.begin();
  dht.begin();
// INICIO DISCO VIRTUAL EEPROM
  EEPROM.begin(sizecadena);//512

  delay(10);
  
  //guardareeprom();
  delay(10);
  leereeprom ();
  delay(10);
  lectura_eprom();
//  recuperardatos_ram_eprom();
  //Serial.println("Booting");
//  boolean cronometro = true;
//  while(cronometro)
//  {
//  Serial.print("10,");
//  if (Serial.available()) {cronometro = false; recuperardatos_ram_eprom();}
//  }
const char* ssid = "CYBORG";
const char* password = "delkano22";

cache = "1,1,x001,NombreIOM,IOM_Garage,IOM_Router,USER,PASS,CASTELLANO,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,";
//WifiSSID.toCharArray(ssid,WifiSSID.length()+1);
//WifiPASS.toCharArray(password,WifiPASS.length()+1);

Serial.println("");
Serial.print("ssid: ");
Serial.println(ssid);
Serial.print("password: ");
Serial.println(password);
//Funcion que espere a recibir cadena string por spi y así configurar el sutup de IOM
  if (!flag)
  {
    char ws [Wifiap.length()];
    Wifiap.toCharArray(ws,Wifiap.length()+1);
    WiFi.softAP(ws);
    myIP = WiFi.softAPIP();
//    Serial.print("AP IP address: ");
//    Serial.println(myIP);
//    Serial.println("Punto de Acceso y Server inicializado");
    Serial.println("");
    Serial.print("AP_ws: ");
    Serial.println(ws);
    Serial.print("AP_Wifiap: ");
    Serial.println(Wifiap);  
    server.on("/", iom);

  }
  else
  {
    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid, password);
 
    while (WiFi.waitForConnectResult() != WL_CONNECTED)
    {
      Serial.println("Connection Failed!");
      delay(5000);
      ESP.restart();
    }
    myIP = WiFi.localIP();

    server.on("/", iom);
  }
  Serial.println("");
  Serial.println( WiFi.hostname() );
  Serial.println( WiFi.SSID() );
  Serial.println( WiFi.psk() );
  Serial.println( WiFi.RSSI() );
  Serial.println( WiFi.macAddress() );
  Serial.println( WiFi.subnetMask()  );
  Serial.println( WiFi.dnsIP()  );
  Serial.println( WiFi.gatewayIP()  );
  Serial.println(myIP);

  geoip();
  IP = String(myIP[0]) + '.' + String(myIP[1]) + '.' + String(myIP[2]) + '.' + String(myIP[3]);
  //SERVIDOR WEB
  //server.on ( "/", handleRoot );
  server.on ( "/index.html" , index_wifi );
  //  server.on ( "/" , index_conexion ); si la bandera es cierta [ flag = true
  //server.on ( "/t.xml" , t_iom );

//  server.on ( "/css/s.css" , s_css );
//  server.on ( "/css/r.css" , r_css  );
//  server.on ( "/js/app.js" , app_js  );
//  server.on ( "/js/s.js" , s_js  );
//  server.on ( "/js/n.js" , n_js  );
//  server.on ( "/js/n_r.js" , nr_js  );
//  server.on ( "/js/n_r_1.js" , r1_js  );
//  server.on ( "/js/n_r_2.js" , r2_js  );
//  server.on ( "/js/n_r_3.js" , r3_js  );
//  server.on ( "/js/n_r_4.js" , r4_js  );
//  server.on ( "/js/loader.js" , loader_js  );
//  server.on ( "/js/g1.js" , g1_js  );
//  server.on ( "/img/antena.svg" , antena_svg );
//  server.on ( "/img/m.svg" , m_svg );
//  server.on ( "/img/favicon.ico" , antena_svg );
//  server.on ( "/img/wifi.svg" , wifi_svg );
//  server.on ( "/img/pass.svg" , pass_svg );
//  server.on ( "/img/tr3.svg" , tr3_svg );
//  server.on ( "/img/hr4.svg" , hr4_svg );
//  server.on ( "/img/e2.svg" , e2_svg );
//  server.on ( "/img/w.svg" , w_svg );
//  server.on ( "/img/w2.svg" , w2_svg );
//  server.on ( "/img/u.svg" , u_svg );
//  server.on ( "/img/u2.svg" , u2_svg );
//  server.on ( "/img/p.svg" , p_svg );
//  server.on ( "/img/p2.svg" , p2_svg );
//  server.on ( "/img/s.svg" , s_svg );
//  server.on ( "/img/s2.svg" , s2_svg );
//  server.on ( "/img/r.svg" , r_svg );
//  server.on ( "/img/r2.svg" , r2_svg );
//  server.on ( "/img/o.svg" , o_svg );
//  server.on ( "/img/o2.svg" , o2_svg );
//  server.on ( "/img/help.svg" , help_svg );
//  server.on ( "/img/off.svg" , off_svg );
  server.on ( "/s.xml" , s_xml );
  server.on ( "/c.xml" , c_xml );
  server.on ( "/g.xml" , g_xml );
  server.on ( "/inline", []() {
  server.send ( 200, "text/plain", "this works as well" );
  } );
  server.onNotFound ( handleNotFound );
  
  //Serial.println ( "HTTP server started" );
  //SENDORES
  

  //RTC
  //rtc.begin(DateTime(F(__DATE__), F(__TIME__)));
  // This line sets the RTC with an explicit date & time, for example to set
  // January 21, 2014 at 3am you would call:
  //rtc.adjust(DateTime(2016, 10, 25, 2,2 , 0));

  //SALIDAS
  pinMode(rele_0, OUTPUT);
  pinMode(rele_1, OUTPUT);
  pinMode(rele_2, OUTPUT);
  pinMode(rele_3, OUTPUT);
  digitalWrite(rele_0, HIGH);
  digitalWrite(rele_1, HIGH);
  digitalWrite(rele_2, HIGH);
  digitalWrite(rele_3, HIGH);

  //Serial.println("Starting UDP");
  Udp.begin(localPort);
  //Serial.print("Local port: ");
  //Serial.println(Udp.localPort());
  //Serial.println("waiting for sync");
  setSyncProvider(getNtpTime);
  Serial.println(Udp.remoteIP() ); 
  
  //SISTEMA OPERATIVO
  //const char ota_pass[] = "0000";
  //ArduinoOTA.setPassword(ota_pass);
  //ArduinoOTA.begin();
  //Serial.println("Funcionando");
  //Serial.print("Esta es la IP del dispositivo: ");
  //Serial.println(WiFi.localIP());
}

void loop()
{
  //Boton de reset flag en false para entrar en modo AP Wifi --> restablecer valores de la memoria a CERO , WifiSsid, User y Pass y permitir ArduinoOTA.handle(); este en modo escucha
  //SERVIDOR WEB
  server.handleClient();

  //SISTEMA OPERATIVO
  //ArduinoOTA.handle();

  //GRAFICAS
  grafica_24h();

  // RT-XT

  //TEMPERATURA
  temperatura ();
  //RELOJ
  reloj();
  //SALIDAS
  salidas();

  //SPI_RAM envia el string de datos al PCB UNO
  spi_ram(); //habilitar 
}
