void salidas()
{
  salida_1();
  salida_2();
  salida_3();
  salida_4();  
}

void salida_1()
{
  if(modo[1] == 1)
    {
        if ( hora >= r[0] && r[1] > hora )
          {
            digitalWrite(rele_0, LOW);
          }
          else
          {
           digitalWrite(rele_0, HIGH);
          }
    }
  else{digitalWrite(rele_0, HIGH);}
}

void salida_2()
{
  if(modo[2] == 1)
  {
      
      if ( (hora == r[2]) and ( (minuto * 60) + segundo <= (r[3] * 60) + r[4] ) )
        {
          digitalWrite(rele_1, LOW);
        }

      else if ( (hora == r[5]) and ( (minuto * 60) + segundo <= (r[6] * 60) + r[7] ) )
        {
          digitalWrite(rele_1, LOW);
        }
 
      else if ( (hora == r[8]) and ( (minuto * 60) + segundo <= (r[9] * 60) + r[10] ) )
        {
          digitalWrite(rele_1, LOW);
        }

      else if ( (hora == r[11]) and ( (minuto * 60) + segundo <= (r[12] * 60) + r[13] ) )
        {
          digitalWrite(rele_1, LOW);
        }
      else
        {
         digitalWrite(rele_1, HIGH);
        }
  }
  else  { digitalWrite(rele_1, HIGH);}
}


void salida_3()
{
  if (  modo[3] == 1 )
    {
      byte tm = r[14] + 1;
      byte tx = r[14] - 1;
      if (t <= tx) {
        digitalWrite(rele_2, LOW);
      }
      if (t >= tm) {
        digitalWrite(rele_2, HIGH);
      }
    }
  else {  digitalWrite(rele_2, HIGH); }
}

void salida_4()
{
  if (  modo[4] == 1 )
    {
      byte hm = r[15] + 1;
      byte hx = r[15] - 1;
      if (h <= hx) {
        digitalWrite(rele_3, LOW);
      }
      if (h >= hm) {
        digitalWrite(rele_3, HIGH);
      }
    }
  else {  digitalWrite(rele_3, HIGH); }
}
